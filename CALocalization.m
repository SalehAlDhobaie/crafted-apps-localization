//
//  CALocalization.m
//  LocalizationPOC
//
//  Created by Saleh AlDhobaie on 10/13/13.
//  Copyright (c) 2013 iCapps. All rights reserved.
//

#import "CALocalization.h"

@interface CALocalization ()

@property (nonatomic, strong) NSArray /* <NSBundle> */ *bundles;

@end


#define CALOCALIZATION_LANGUAGE_KEY @"CALocalization"

@implementation CALocalization

+ (instancetype)sharedInstance {
    
    static CALocalization *_sharedLocalSystem = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once( &onceToken, ^{
        _sharedLocalSystem = [[CALocalization alloc] initWithLanguage];
    });
    
    return _sharedLocalSystem;
}

- (id) initWithLanguage {
    
    self = [super init];
    if(self) {
        
        NSString *language = [[NSUserDefaults standardUserDefaults] objectForKey:CALOCALIZATION_LANGUAGE_KEY];
        
        if ( language && ![language isEqualToString:@""] ) {
            CALocalizationLanguage option = [self CALocalizationLanguageFromString:language];
            [self setLanguageWithOption:option];
        }else {
            [self DefaultLanguage];
        }
    }
    return self;
}

- (id)init {
    @throw @"Please use the singleton.";
}

// Gets the current localized string as in NSLocalizedString.
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName {
    
    NSString *message = nil;
    for( NSBundle *bundle in self.bundles ) {
        NSLog(@"%@",bundle);
        
        message = [bundle localizedStringForKey:key value:value table:tableName];
        if( ![message isEqualToString:key] && ![message isEqualToString:value]) {
            NSLog(@"message :%@", message);
            NSLog(@"value   :%@", value);
            NSLog(@"key     :%@", key);
            
            return message;
        }
    }
    return message;
}



- (void)setLanguageWithOption:(CALocalizationLanguage)languageOption {
    
    NSString *language = [self stringFromCALocalizationLanguage:languageOption];
    [[NSUserDefaults standardUserDefaults] setObject:language forKey:CALOCALIZATION_LANGUAGE_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableArray *languages = [[NSMutableArray alloc]initWithCapacity:3];
	NSString *path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];

    if (path != nil) {
        [languages addObject:[NSBundle bundleWithPath:path]];
    }
    
    // try the neutral culture bundle
	if ([language rangeOfString:@"-"].location != NSNotFound) {
		
        language = [[language componentsSeparatedByString:@"-"]objectAtIndex:0];
        // fallback
        path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj" ];
        
        if (path != nil) {
            [languages addObject:[NSBundle bundleWithPath:path]];
        }
    }
    
    [languages addObject:[NSBundle mainBundle]];
    self.bundles = languages;
}

/**
 @param Resets the localization system, so it uses the OS default language.
 */
- (void) DefaultLanguage {
	self.bundles = @[[NSBundle mainBundle]];
    
}


- (NSString *)stringFromCALocalizationLanguage:(CALocalizationLanguage) language {

    NSString *stringLanguage = nil;
    
    switch (language) {
        case CALocalizationLanguageFrench :
            stringLanguage = @"fr";
            break;
        case CALocalizationLanguageEnglish :
            stringLanguage = @"en";
            break;
        case CALocalizationLanguageGerman :
            stringLanguage = @"de";
            break;
        case CALocalizationLanguageJapanese :
            stringLanguage = @"ja";
            break;
        case CALocalizationLanguageDutch :
            stringLanguage = @"nl";
            break;
        case CALocalizationLanguageItalian :
            stringLanguage = @"it";
            break;
        case CALocalizationLanguageSpanish :
            stringLanguage = @"es";
            break;
        case CALocalizationLanguagePortuguese :
            stringLanguage = @"pt";
            break;
        case CALocalizationLanguagePortuguese_Portugal :
            stringLanguage = @"\"pt-PT\"";
            break;
        case CALocalizationLanguageDanish :
            stringLanguage = @"da";
            break;
        case CALocalizationLanguageFinnish :
            stringLanguage = @"fi";
            break;
        case CALocalizationLanguageNorwegianBokmal :
            stringLanguage = @"nb";
            break;
        case CALocalizationLanguageSwedish :
            stringLanguage = @"sv";
            break;
        case CALocalizationLanguageKorean :
            stringLanguage = @"ko";
            break;
        case CALocalizationLanguageChinese_Simplified :
            stringLanguage = @"\"zh-Hans\"";
            break;
        case CALocalizationLanguageChinese_Traditional :
            stringLanguage = @"\"zh-Hant\"";
            break;
        case CALocalizationLanguageRussian :
            stringLanguage = @"ru";
            break;
        case CALocalizationLanguagePolish :
            stringLanguage = @"pl";
            break;
        case CALocalizationLanguageTurkish :
            stringLanguage = @"tr";
            break;
        case CALocalizationLanguageUkrainian :
            stringLanguage = @"uk";
            break;
        case CALocalizationLanguageArabic :
            stringLanguage = @"ar";
            break;
        case CALocalizationLanguageCroatian :
            stringLanguage = @"hr";
            break;
        case CALocalizationLanguageCzech :
            stringLanguage = @"cs";
            break;
        case CALocalizationLanguageGreek :
            stringLanguage = @"el";
            break;
        case CALocalizationLanguageHebrew :
            stringLanguage = @"he";
            break;
        case CALocalizationLanguageRomanian :
            stringLanguage = @"ro";
            break;
        case CALocalizationLanguageSlovak :
            stringLanguage = @"sk";
            break;
        case CALocalizationLanguageThai :
            stringLanguage = @"th";
            break;
        case CALocalizationLanguageIndonesian :
            stringLanguage = @"id";
            break;
        case CALocalizationLanguageMalay :
            stringLanguage = @"ms";
            break;
        case CALocalizationLanguageEnglish_UnitedKingdom :
            stringLanguage = @"\"en-GB\"";
            break;
        case CALocalizationLanguageCatalan :
            stringLanguage = @"ca";
            break;
        case CALocalizationLanguageHungarian :
            stringLanguage = @"hu";
            break;
        case CALocalizationLanguageVietnamese :
            stringLanguage = @"vi";
            break;
        default:
            break;
    }
    return stringLanguage;

}



- (NSUInteger)CALocalizationLanguageFromString:(NSString *)language{
    /*
    return @{
            @(CALocalizationLanguageFrench)                     : @"fr",
            @(CALocalizationLanguageEnglish)                    : @"en",
            @(CALocalizationLanguageGerman)                     : @"de",
            @(CALocalizationLanguageJapanese)                   : @"ja",
            @(CALocalizationLanguageDutch)                      : @"nl",
            @(CALocalizationLanguageItalian)                    : @"it",
            @(CALocalizationLanguageSpanish)                    : @"es",
            @(CALocalizationLanguagePortuguese)                 : @"pt",
            @(CALocalizationLanguagePortuguese_Portugal)        : @"\"pt-PT\"",
            @(CALocalizationLanguageDanish)                     : @"da",
            @(CALocalizationLanguageFinnish)                    : @"fi",
            @(CALocalizationLanguageNorwegianBokmal)            : @"nb",
            @(CALocalizationLanguageSwedish)                    : @"sv",
            @(CALocalizationLanguageKorean)                     : @"ko",
            @(CALocalizationLanguageChinese_Simplified)         : @"\"zh-Hans\"",
            @(CALocalizationLanguageChinese_Traditional)        : @"\"zh-Hant\"",
            @(CALocalizationLanguageRussian)                    : @"ru",
            @(CALocalizationLanguagePolish)                     : @"pl",
            @(CALocalizationLanguageTurkish)                    : @"tr",
            @(CALocalizationLanguageUkrainian)                  : @"uk",
            @(CALocalizationLanguageArabic)                     : @"ar",
            @(CALocalizationLanguageCroatian)                   : @"hr",
            @(CALocalizationLanguageCzech)                      : @"cs",
            @(CALocalizationLanguageGreek)                      : @"el",
            @(CALocalizationLanguageHebrew)                     : @"he",
            @(CALocalizationLanguageRomanian)                   : @"ro",
            @(CALocalizationLanguageSlovak)                     : @"sk",
            @(CALocalizationLanguageThai)                       : @"th",
            @(CALocalizationLanguageIndonesian)                 : @"id",
            @(CALocalizationLanguageMalay)                      : @"ms",
            @(CALocalizationLanguageEnglish_UnitedKingdom)      : @"\"en-GB\"",
            @(CALocalizationLanguageCatalan)                    : @"ca",
            @(CALocalizationLanguageHungarian)                  : @"hu",
            @(CALocalizationLanguageVietnamese)                 : @"vi"
            };
     */
    NSDictionary *typeFromString = @{
                                     
             @"fr"  :            @(CALocalizationLanguageFrench),
             @"en"  :            @(CALocalizationLanguageEnglish),
             @"de"  :            @(CALocalizationLanguageGerman),
             @"ja"  :            @(CALocalizationLanguageJapanese),
             @"nl"  :            @(CALocalizationLanguageDutch),
             @"it"  :            @(CALocalizationLanguageItalian),
             @"es"  :            @(CALocalizationLanguageSpanish),
             @"pt"  :            @(CALocalizationLanguagePortuguese),
             @"fi"  :            @(CALocalizationLanguageFinnish),
             @"nb"  :            @(CALocalizationLanguageNorwegianBokmal),
             @"sv"  :            @(CALocalizationLanguageSwedish),
             @"ko"  :            @(CALocalizationLanguageKorean),
             @"\"zh-Hans\"" :    @(CALocalizationLanguageChinese_Simplified),
             @"\"zh-Hant\"" :    @(CALocalizationLanguageChinese_Traditional),
             @"ru"  :            @(CALocalizationLanguageRussian),
             @"pl"  :            @(CALocalizationLanguagePolish),
             @"tr"  :            @(CALocalizationLanguageTurkish),
             @"uk"  :            @(CALocalizationLanguageUkrainian),
             @"ar"  :            @(CALocalizationLanguageArabic),
             @"hr"  :            @(CALocalizationLanguageCroatian),
             @"cs"  :            @(CALocalizationLanguageCzech),
             @"el"  :            @(CALocalizationLanguageGreek),
             @"he"  :            @(CALocalizationLanguageHebrew),
             @"ro"  :            @(CALocalizationLanguageRomanian),
             @"sk"  :            @(CALocalizationLanguageSlovak),
             @"th"  :            @(CALocalizationLanguageThai),
             @"id"  :            @(CALocalizationLanguageIndonesian),
             @"ms"  :            @(CALocalizationLanguageMalay),
             @"\"en-GB\""   :    @(CALocalizationLanguageEnglish_UnitedKingdom),
             @"ca"  :            @(CALocalizationLanguageCatalan),
             @"hu"  :            @(CALocalizationLanguageHungarian),
             @"vi"  :            @(CALocalizationLanguageVietnamese)
             
            };
    return [typeFromString[language]integerValue];
}








@end
