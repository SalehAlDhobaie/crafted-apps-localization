//
//  CALocalization.h
//  LocalizationPOC
//
//  Created by Saleh AlDhobaie on 10/13/13.
//  Copyright (c) 2013 iCapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>



/*
 All these Option represent to list of abbreviation ..
 "fr, French",
 "en, English",
 "de, German",
 "ja, Japanese",
 "nl, Dutch",
 "it, Italian",
 "es, Spanish",
 "pt, Portuguese",
 "pt-PT, Portuguese (Portugal)",
 "da, Danish",
 "fi, Finnish",
 "nb, Norwegian Bokm\U221a\U2022l",
 "sv, Swedish",
 "ko, Korean",
 "zh-Hans, Chinese (Simplified)",
 "zh-Hant, Chinese (Traditional)",
 "ru, Russian",
 "pl, Polish",
 "tr, Turkish",
 "uk, Ukrainian",
 "ar, Arabic",
 "hr, Croatian",
 "cs, Czech",
 "el, Greek",
 "he, Hebrew",
 "ro, Romanian",
 "sk, Slovak",
 "th, Thai",
 "id, Indonesian",
 "ms, Malay",
 "en-GB, English (United Kingdom)",
 "ca, Catalan",
 "hu, Hungarian",
 "vi, Vietnamese"
 */



/**
 @param CALocalizationLanguage to all languages that support from Apple
 */
typedef NS_ENUM(NSUInteger, CALocalizationLanguage){
    
    CALocalizationLanguageFrench,
    CALocalizationLanguageEnglish,
    CALocalizationLanguageGerman,
    CALocalizationLanguageJapanese,
    CALocalizationLanguageDutch,
    CALocalizationLanguageItalian,
    CALocalizationLanguageSpanish,
    CALocalizationLanguagePortuguese,
    CALocalizationLanguagePortuguese_Portugal,
    CALocalizationLanguageDanish,
    CALocalizationLanguageFinnish,
    CALocalizationLanguageNorwegianBokmal,
    CALocalizationLanguageSwedish,
    CALocalizationLanguageKorean,
    CALocalizationLanguageChinese_Simplified,
    CALocalizationLanguageChinese_Traditional,
    CALocalizationLanguageRussian,
    CALocalizationLanguagePolish,
    CALocalizationLanguageTurkish,
    CALocalizationLanguageUkrainian,
    CALocalizationLanguageArabic,
    CALocalizationLanguageCroatian,
    CALocalizationLanguageCzech,
    CALocalizationLanguageGreek,
    CALocalizationLanguageHebrew,
    CALocalizationLanguageRomanian,
    CALocalizationLanguageSlovak,
    CALocalizationLanguageThai,
    CALocalizationLanguageIndonesian,
    CALocalizationLanguageMalay,
    CALocalizationLanguageEnglish_UnitedKingdom,
    CALocalizationLanguageCatalan,
    CALocalizationLanguageHungarian,
    CALocalizationLanguageVietnamese
    
};


@interface CALocalization : NSObject


/**
 @param Like NSLocalizedString just write key, comment (comment accept nil).
 */
#define CALocalizedString(key, comment) [[CALocalization sharedInstance] localizedStringForKey:(key) value:@"" table:nil]


/**
 @param setting language of Localization based on CALocalizationLanguage option
 */
#define CALocalizationSetLanguageOption(language) [[CALocalization sharedInstance] setLanguageWithOption:(language)]

/**
 @param reset Loclaization language based on main bundle.
 */
#define CALocalizationDefaultLanguage [[CALocalization sharedInstance] DefaultLanguage]


+ (instancetype)sharedInstance;

// gets the string localized
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName;

// Setting Language to app
- (void)setLanguageWithOption:(CALocalizationLanguage)languageOption;

/**
 @param reset Language to Main Bundle Language.
 */
- (void) DefaultLanguage;

@end
