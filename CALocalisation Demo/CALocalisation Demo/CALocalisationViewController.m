//
//  CALocalisationViewController.m
//  CALocalisation Demo
//
//  Created by Saleh AlDhobaie on 10/18/13.
//  Copyright (c) 2013 Saleh AlDhobaie. All rights reserved.
//

#import "CALocalisationViewController.h"

@interface CALocalisationViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation CALocalisationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"%@", CALocalizedString(@"Hello World!", nil));
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.label.text = [NSString stringWithFormat:@"Language is : %@", CALocalizedString(@"Language", nil)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
