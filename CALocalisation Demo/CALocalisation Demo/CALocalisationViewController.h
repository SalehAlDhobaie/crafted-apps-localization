//
//  CALocalisationViewController.h
//  CALocalisation Demo
//
//  Created by Saleh AlDhobaie on 10/18/13.
//  Copyright (c) 2013 Saleh AlDhobaie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CALocalization.h"

@interface CALocalisationViewController : UIViewController

@end
