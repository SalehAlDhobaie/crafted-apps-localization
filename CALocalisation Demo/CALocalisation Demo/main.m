//
//  main.m
//  CALocalisation Demo
//
//  Created by Saleh AlDhobaie on 10/18/13.
//  Copyright (c) 2013 Saleh AlDhobaie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CALocalisationAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CALocalisationAppDelegate class]));
    }
}
