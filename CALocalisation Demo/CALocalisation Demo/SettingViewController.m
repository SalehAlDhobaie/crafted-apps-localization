//
//  SettingViewController.m
//  CALocalisation Demo
//
//  Created by Saleh AlDhobaie on 10/18/13.
//  Copyright (c) 2013 Saleh AlDhobaie. All rights reserved.
//

#import "SettingViewController.h"
#import "CALocalization.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)changeLanguage:(UISwitch *)sender {
    
    if (sender.isOn) {
        CALocalizationSetLanguageOption(CALocalizationLanguageFrench);
    }else {
        CALocalizationSetLanguageOption(CALocalizationLanguageEnglish);
    }
    
}
- (IBAction)doneButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
